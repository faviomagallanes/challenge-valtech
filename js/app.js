window.addEventListener("DOMContentLoaded", (event) => {
  const API = "https://www.mockachino.com/a71b232c-218e-4d/users";
  const cardContainer = document.querySelector(".card-container");
  const alert_error = document.querySelector(".alert-error");
  const totalUsers = document.getElementById("totalUsers");
  const selectTypeFilter = document.getElementById("select-filter");
  const pagination_element = document.getElementById("pagination");
  const search = document.getElementById("search");

  let respData;
  /* 
  setDataAPIToLocalStorage()
   Función que coloca el resultado de la api en el Local Storage, para mejorar el funcionamiento
   y no hacer un llamado cada vez que se quieran consumir los datos.
*/
  const setDataAPIToLocalStorage = async (API) => {
    getDataAPIToLocalStorage();
    if (respData === null) {
      const resp = await fetch(API);
      const data = await resp.json();
      location.reload();
      window.localStorage.setItem("dataAPI", JSON.stringify(data));
    }
  };

  /*
    getDataAPIToLocalStorage()
    Función que trae la data ya almacenada en el Local Storage
  */
  const getDataAPIToLocalStorage = () => {
    respData = window.localStorage.getItem("dataAPI");
    respData = JSON.parse(respData);

    return respData?.results;
  };

  let current_page = 1;
  let rows = 10;

  /*
    createCard()
    Función que crea las cards
  */
  const createCard = (data) => {
    const { picture, name, location, registered, email } = data;

    const userEl = document.createElement("div");
    userEl.classList.add("card");
    userEl.innerHTML = `      
          <img
            src="${picture.large}"
          />
          <div class="card-info">
                <h2>${name.first} ${name.last}</h2>
                <i class="fas fa-envelope">
                <small>${email}</small>
                </i>
                <i class="fas fa-map-marker-alt">
                  <small>${location.city} ${location.state}</small>
                </i>
          </div>
          <hr />
          <div class="favorite-container">
            <i class="far fa-clock clock"><span>${
              registered.age + "M"
            } ago</span> </i>
            <i  class="fas fa-heart heart-icon"></i>
          </div>    
      `;
    cardContainer.appendChild(userEl);
  };

  /*
    addClassHeart()
    Función que toma todas las clases (.heart-icon) de las cards y les agrega el event 'click', para así poder
    marcar los usuarios como "favoritos"
  */
  const addClassHeart = () => {
    const iconsTotalHearts = document.querySelectorAll(".heart-icon");

    iconsTotalHearts.forEach((icon) => {
      icon.addEventListener("click", () => {
        !icon.classList.contains("red")
          ? icon.classList.add("red")
          : icon.classList.remove("red");
      });
    });
  };

  /* 
    filterByAscAndDesc()
    Función que permite filtrar de forma ascendente o descendente
  */
  const filterByAscAndDesc = () => {
    selectTypeFilter.addEventListener("change", () => {
      current_page = 1;
      search.value = "";
      const valueFilter = selectTypeFilter.value;

      valueFilter === "ascendant"
        ? DisplayList(
            orderAscendant(getDataAPIToLocalStorage()),
            cardContainer,
            rows,
            current_page
          )
        : DisplayList(
            orderDescendant(getDataAPIToLocalStorage()),
            cardContainer,
            rows,
            current_page
          );
    });
  };

  /*
  debounce()
  Función que ayuda a disparar el evento Keyup en el input search con un retraso
  para que no se ejecute cada vez que hay una entrada en el input.
*/
  const debounce = (callback, wait) => {
    let timerId;
    return (...args) => {
      clearTimeout(timerId);
      timerId = setTimeout(() => {
        callback(...args);
      }, wait);
    };
  };

  /*
  orderAscendant()
  Función con la condición para una búsqueda ascendente
*/
  const orderAscendant = (arr) => {
    let filterDesc = arr.sort((a, b) => {
      if (a.name.first < b.name.first) {
        return 1;
      }
      if (a.name.first > b.name.first) {
        return -1;
      }
      return 0;
    });

    return filterDesc;
  };

  /*
  orderDescendant()
  Función con la condición para una búsqueda descendente
*/
  function orderDescendant(arr) {
    let filterDesc = arr.sort((a, b) => {
      if (a.name.first < b.name.first) {
        return -1;
      }
      if (a.name.first > b.name.first) {
        return 1;
      }
      return 0;
    });
    return filterDesc;
  }

  /*
    searchUsers()
    Función que busca los usuarios, cada vez que se pulsa una tecla filtra en tiempo real
  */
  const searchUsers = () => {
    search.addEventListener(
      "keyup",
      debounce(() => {
        current_page = 1;
        alert_error.innerHTML = "";

        if (search.value !== "") {
          let data = getDataAPIToLocalStorage().filter((user) =>
            user.name.first.toLowerCase().startsWith(search.value.toLowerCase())
          );

          if (data.length !== 0) {
            DisplayList(data, cardContainer, rows, current_page);
          } else {
            cardContainer.innerHTML = "";
            pagination_element.innerHTML = "";
            totalUsers.innerHTML = `Showing 0 of<span>0 candidates</span> `;
            createdAlertError("Not result match", alert_error);
          }
        } else {
          DisplayList(
            getDataAPIToLocalStorage(),
            cardContainer,
            rows,
            current_page
          );
        }
      }, 500)
    );
  };

  const createdAlertError = (msg, wrapper) => {
    const msgError = document.createElement("p");
    msgError.classList.add("red");
    msgError.classList.add("alert");
    msgError.innerHTML = msg;
    wrapper.appendChild(msgError);
  };

  // PAGINATION
  const DisplayList = (items, wrapper, rows_per_page, page) => {
    wrapper.innerHTML = "";

    let arrToShow = items.slice(
      page > 1 ? rows_per_page * (page - 1) : 0,
      rows_per_page * page
    );

    totalUsers.innerHTML = `Showing ${arrToShow.length * page} of<span>${
      items.length
    } candidates</span> `;

    for (let i = 0; i < arrToShow.length; i++) {
      createCard(arrToShow[i]);
    }

    addClassHeart();
    // Agrego la paginacion
    SetupPagination(items, pagination_element, rows);
  };

  const SetupPagination = (items, wrapper, rows_per_page) => {
    wrapper.innerHTML = "";

    let page_count = Math.ceil(items.length / rows_per_page);
    for (let i = 1; i < page_count + 1; i++) {
      let btn = PaginationButton(i, items);
      wrapper.appendChild(btn);
    }
  };

  const PaginationButton = (page, items) => {
    let button = document.createElement("button");
    button.innerText = page;

    if (current_page == page) button.classList.add("active");

    button.addEventListener("click", function () {
      current_page = page;
      DisplayList(items, cardContainer, rows, current_page);

      let current_btn = document.querySelector(".active");

      if (!current_btn.classList.contains("active")) {
        current_btn.classList.remove("active");
        button.classList.add("active");
      }

      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    });

    return button;
  };

  setDataAPIToLocalStorage(API);

  DisplayList(getDataAPIToLocalStorage(), cardContainer, rows, current_page);

  //Filter User
  filterByAscAndDesc();
  searchUsers();
});
